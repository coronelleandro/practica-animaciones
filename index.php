<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/pokeball.css">
    <link rel="stylesheet" href="css/card.css">
    <link >
    <title>Hello, world!</title>
  </head>
  <body>
    <header class="header">
      <div class="divMenu">
        <div class="divLogo">
        </div>
        <div id="hamburger">
            <button class="button">
                <span class="lines topLine"></span>
                <span class="lines middleLine"></span>
                <span class="lines bottomLine"></span>
            </button>
        </div>
      </div>
    </header>

    <?php
      require_once("section/pokeball.php");
      require_once("section/card.php");
    ?>

    <footer class="footer">
       Copyrigth2020
    </footer>
    <script type="text/javascript" src="js/index.js"></script>
    <script type="text/javascript" src="js/pokeball.js"></script>
  </body>
</html>