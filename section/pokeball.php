<section class="sectionPokeball">
    <article class="artPokeball">
        <div id="pokeball" class="pokeball">
            <div class="pokeballButton">
                <div id="interButtton" class="interButtton"></div>
            </div>    
            <div class="line"></div>
            <div class="red"></div>
            <div class="white"></div> 
        </div>
    </article>
</section>